---
layout: essay
type: essay
title: Good Code Doesn't Exist
date: 2020-5-14
labels:
  - Software Development
---

<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>

If there's one thing I learned in ICS314, it's that less is more when it comes
to writing software. All else being equal, it is almost always better to write
fewer lines of code. In fact, many, if not most, fundamental software
engineering techniques are really techniques to reduce the amount of code we
have to write.

## Coding Standards

Coding standards are rules regarding how software should be formatted and
written that is automatically checked by a program, such as eslint. Most coding
standards warn against writing code that is unecesarily long or just unecessary.
Consider the following code:

```javascript
const foo = () => {
  return {
    a: 1,
    b: 2,
  };
};

```

eslint configured with the coding standards for ICS314 will produce the
following errors:

```
28:7   error  'foo' is assigned a value but never used no-unused-vars
28:19  error  Unexpected block statement surrounding arrow body; parenthesize the returned value and move it immediately after the `=>`  arrow-body-style
```

Both of these errors represent code that is either unecessarily long or doesn't
need to exist, and that's not an accident. eslint is much happier with the
shorter version with less code:

```javascript
const foo = () => ({
  a: 1,
  b: 2,
});
```

But best of all is:

```javascript
```

## User Interface Frameworks

User Interface Frameworks are one of the most obvious examples of a software
engineering technique to reduce the amount code you have to write.  A bad
software developer will write 10 different pieces of HTML, JavaScript, and CSS
every time they work on a website that has 10 buttons. A better software
developer will write one, and reuse it 10 times. The best software developer
will write zero, and use a UI library that has written one for them.

## Design Patterns

Design Patterns are standard solutions to common problems software developers
face, such as creating objects or keeping track of state.  Design patterns very
frequently help you avoid writing code, especially compared to an ad hoc
solution.
