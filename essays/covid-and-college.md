---
layout: essay
type: essay
title: "Covid and College"
date: 2020-04-03
labels:
  - Covid-19
---

The global Covid-19 pandemic has usurped my life in many ways.

## Fear

By far the biggest impact this pandemic has had on my life is how it causes me
to worry about my friends and family. Both of my parents are physicians, and
both have seen patients who have contracted Covid-19. My father is a doctor in
New York, and he tells me he had symptoms of the disease. Currently all of the
doctors at his workplace are seeing patients remotely. I have two younger
brothers, one who lives with my mother and one who lives with my father, so I
also worry about them as well.

A few days ago my girlfriend sprained her ankle after stepping into one of
Honolulu's many pot holes.  I brought her to a hospital, and we were both
worried because they were testing patients for Covid-19 at the location.

## School

Another large impact the pandemic has had is on my school. All of the classes
at UH Manoa have been moved to online only. While at first, I thought this may
actually be convienient, at the time I underestimated how hard it can be to do
so much work with all of the distractions around me at home.

It isn't only academics that have been impacted by the pandemic, but also
extracurricular activities as well. This is the first semester of the DevDev
club, which is an ACM SIG that focuses on "Developing Developers" by having
them work on personal software projects. While we have been able to meet
online, it has been off to a slow start because of the pandemic.

## Conclusion

Overall, I think the pandemic shows how fragile society has become.  We are all
dependant on things far beyond our control, and fequently, our comprehension.
Things like supply chains for goods and services, the stock market, and public
policy. I strongly believe this disease is not the end of the world as we know
it, but it is certainly a sobering wake up call as to out place in the world.
