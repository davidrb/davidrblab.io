---
layout: essay
type: essay
title: There is Only One Design Pattern
date: 2018-04-30
labels:
  - Software Engineering
  - Functional Programming
  - C++
  - Clojure
---
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
In software engineering, a software design pattern is a general, reusable
solution used by programmers to get around their language's lack of support for
functional programming.

When you are holding a hammer, everything looks like a nail.  When you are
programming in an Object Oriented language, everything looks like a class.
Programmers using these languages often write a class when they should just be
writing functions, probably due to the fact that Java and C++ did not always
support lambda expressions. In my opinion, many design patterns are an example
of this.

Design patterns are commonly catagorized into three catagories: creational,
structural, and behavioral. In this essay I will give an example of each,
discuss when I or others have used it, and explain how it is really just a
instance of the one and only true design pattern: Higher order functions.

Note: I use c++ in this essay. `[&](){ /* do something */ }` is c++ syntax
for a lambda expression.

## Creational Patterns

Creational design patterns are patterns used for creating objects. 

### The Factory Method Pattern

The most basic creational pattern is the factory method pattern, which allows
you to create objects without specifying their type. The type to use can be
specified by inheriting from the base class and overriding the factory method.

I have used this design pattern before when programming a game engine. My game
engine supported two different graphics APIs: OpenGL and Direct3D.  It also
allowed choosing which API to use at runtime, without recompiling the program.
It acheived this using the factory method pattern. The code looked something
like the following:

```cpp
class GameEngine {
public:
  virtual auto createRenderer() -> unique_ptr<Renderer> = 0;

  void play(Game& game) { /* run game loop */ }

  virtual ~GameEngine() {}

private:
  // Defer creation of renderer to subclasses
  unique_prt<Renderer> renderer = createRenderer();
};

class OpenGLEngine : public GameEngine {
public:
  auto createRenderer() -> unique_ptr<Renderer> override {
    return make_unique<OpenGLRenderer>();
  }
};

class D3DEngine : public GameEngine {
public:
  auto createRenderer() -> unique_ptr<Renderer> override {
    return make_unique<D3DRenderer>();
  }
};

int main(int argc, char **argv) {
  auto engine = unique_ptr<GameEngine>{nullptr};

  if (string{argv[1]} == "OpenGL") {
    engine = make_unique<OpenGLEngine>();
  } else if (string{argv[1]} == "Direct3D") {
    engine = make_unique<D3DEngine>();
  } else {
    // choose a default
  }

  engine->play(MyGame{});
}
```

\(Apologies for the C++\)

This pattern parameterizes our `Engine` class by a method which constructs one
of its dependencies. Instead of subclassing and overriding this method, we
could have passed it in as a higher order function instead:

```cpp
class GameEngine {
public:
  GameEngine(function<unique_ptr<Renderer>()> createRenderer) :
    createRenderer{ createRenderer }
    renderer{ createRenderer() } {
  }

private:
  function<unique_ptr<Renderer>()> createRenderer;
  unique_ptr<Renderer> renderer;
};
```

## Behavioral Pattern

Behavioral patterns implement communication patterns between objects.

### The Command Pattern

The Command pattern encapsulates an action into an object, allowing it to be
invoked at a later time. I can't actually recall a time I've used this, nor can
I think of a time I would, because it is obviously just a closure.

Each action has a corresponding class, which stores all the information needed
to perform that action, and a single method, `execute` that executes that
action.

```cpp
class Action {
public:
  virtual void execute() = 0;
  virtual ~Action() {};
};

class MyAction : public Action {
  Action(Receiver& receiver, Param param) :
    receiver{ receiver }, param{ param } {
  }

  void execute() override {
    receiver.do_something(param);
  }

private:
  Receiver& receiver;
  Param param;
}; // Barf

int main() {
  Receiver receiver;
  auto action = make_unique<MyAction>(receiver, 3);
  // ...
  action->execute();
}
```

The most common symptom of class overuse is having a single public method, named
something like 'execute'. These classes are obviously just a thin veneer over a
closure.

```cpp
int main() {
  Receiver receiver;
  auto action = [&receiver](){ receiver.do_something(3); }
  // ...
  action();
}
```

One way this pattern is used is to implement undo functionality. In this case,
an `Action` is given an `undo` method, and actions are pushed onto a stack as 
they are executed. To undo, an action is popped off the stack and its `undo` method
is called. This can also be implemented better by using closures:

```cpp
int main() {
  Receiver receiver;
  auto undos = vector<function<void()>>{};

  auto action = [&](){
    receiver.do_something(3); 

    undos.push_back([&](){
      receiver.undo_something(3);
    });
  };

  action(); // Do something
  // ...
  undos.back()(); // Undo something
  undos.pop_back();
}
```
## Structural Patterns

Structural design patterns help realize relationships between entities.

### The Decorator Pattern

The decorator pattern allows behavior to be added to an object dynamically at
runtime, using composition instead of inheritance.

An example of when this would be useful is in an HTTP server.  HTTP frameworks
often allow functionality to be added dynamically, by wrapping an application
in 'middleware' that adds features such as authentication, csrf protection,
logging, etc. in a reusable manner. These middleware form a chain ending with
the application's request handler, with each link modifying the request, or
possibly short circuiting the whole thing, as in an authentication middleware.

```cpp
class Handler {
public:
  virtual auto handle(Request const& request) -> Response = 0;
  virtual ~Handler(){};
};

class BaseApp : public Handler {
public:
  auto handle(Request const& request) -> Response override {
    return Response{200}; // Success
  }
};

class AuthMiddleware : public Handler {
public:
  AuthMiddleware(unique_ptr<Handler> handler) :
    handler{move(handler)} {
  }

  auto handle(Request const& request) -> Response override {
    if(is_user_authorized(req))
      return handler->handle(req);
    else
      return Response{403}; // Unauthorized
  }

private:
  bool is_user_authorized(Request const& req) {
    return /* check if user is authorized */;
  }

  unique_ptr<Handler> handler;
};

int main() {
  auto app = AuthMiddleware{make_unique<BaseApp>()};
  // ...
  auto response = app.handle(request);
}
```

This class also has a single public method, this time named `handle`.  Once
again, we can acheive this functionality better by using higher order
functions.

```cpp
int main() {
  using Handler = function<Response(Request const&)>;

  auto base_app = [](Request const& req) {
    return Response{200};
  };

  auto wrap_auth = [](Handler handler) {
    return [=](Request const& req) {
      if(is_user_authorized(req))
        return handler(req);
      else
        return Response{403};
    };
  };

  auto app = wrap_auth(base_app);
  // ...
  auto response = app(request);
}
```

Clojure has a very nice library called
[ring](https://github.com/ring-clojure/ring), which
[implements](https://github.com/ring-clojure/ring/wiki/Concepts#middleware)
this in lisp:

```clj
(defn wrap-auth [handler]
  (fn [request]
    (if (authorized? request)
      (handler request)
      (-> (response "Access Denied")
          (status 403)))))
```

## Stop Writing Classes

A great talk was given at PyCon 2012 that makes many of the same points I do in
this essay.

<iframe width="560" height="315"
src="https://www.youtube.com/embed/o9pEzgHorH0" frameborder="0"
allow="autoplay; encrypted-media" allowfullscreen class='youtube'></iframe>
