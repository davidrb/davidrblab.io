---
layout: essay
type: essay
title: Why Object-Oriented Polymorphism Sucks and How to Fix it
date: 2018-05-09
labels:
  - Software Engineering
  - C++
  - Java
  - JavaScript
  - Clojure
  - Ruby
  - Functional Programming
---

Polymorphism, along with encapsulation, abstraction, and inheritance, are often
described as the defining features of object oriented programming languages.
But the way traditional object-oriented programming languages such as C++ and
Java implement polymorphism leaves much to be desired. This essay will discuss
some of these shortcomings, and explore ways more modern object-oriented
languages, as well as functional languages, attempt to fix them.

## What is Polymorphism?

Polymorphism is the ability of a programming language to choose between several
different implementations depending on context, usually the type of an object
or some other value. Traditional object oriented languages implement
polymorphism by allowing programmers to _inherit_ from a class and override
that class' methods. For example,

```java
public interface Cat {
  public String meow();
}

/** A cat that speaks English */
public class AmericanCat implements Cat {
  @Override
  public String meow() {
    return "meow!";
  }
}

/** A cat that speaks Japanese */
public class JapaneseCat {
  @Override
  public String meow() {
    return "nyaa!";
  }
}
```

Calling `.meow` on a `Cat` will return either "meow!" or "nyaa!" depending on
the concrete type of the object. This is an example of _dynamic dispatch_.
Unfortunately, Java-style dynamic dispatch is far from perfect.

## Single vs. Multiple Dispatch

One problem with traditional object oriented polymorphism is that it can only
dispatch on _one_ type, that is, the type of the object we are calling the
method on. It's not hard to find situations where we would like to dispatch
on the type of _multiple_ objects, say one of the parameters. In fact, all you
need to do is look up examples of the _visitor_ design pattern, which is really
just a way of simulating multiple dispatch in langauges that don't support it.

```java
public interface AnimalVisitor {
  public String interact(Dog dog);
  public String interact(Cat cat);
  // ...
}

public interface Animal {
  public void accept(AnimalVisitor visitor);
}

public class Dog implements Animal, AnimalVisitor {
  public void accept(AnimalVisitor) {
    AnimalVisitor.interact(this);
  }

  public String interact(Dog dog) {
    return "The dog sniffs the other dogs butt.";
  }

  public String interact(Cat cat) {
    return "The dog barks at the cat.";
  }
}

public class Cat implements Animal, AnimalVisitor {
  public void accept(AnimalVisitor) {
    AnimalVisitor.interact(this);
  }

  public String interact(Dog dog) {
    return "The cat hisses at the dog.";
  }

  public String interact(Cat cat) {
    return "The cat meows at the other cat.";
  }
}
```

Now we can do this,

```java
Animal spot = new Dog();
Animal bella = new Cat();

spot.interact(bella); // => "The dog barks at the cat."
bella.interact(dog); // => "The cat hisses at the dog."
```

What we really would like to do is just dispatch on the type of the parameter
in addition to the type of the `Animal`.
One solution to this problem is called _open multimethods_. Open multimethods
allows the programmer to specify what value to dispatch on.

One language that supports this feature is _clojure_. We can even use clojure's
Java interop capabilities to use the above class heirarchy directly,

```clojure
; dispatch on the type of both arguments
(defmulti interact (fn [x y] [(class x) (class y)]))

(defmethod interact [Dog Dog]
  "The dog sniffs the other dog's butt.")

(defmethod interact [Dog Cat]
  "The dog barks at the cat.")

(defmethod interact [Cat Cat]
  "The cat meows at the other cat.")

(defmethod interact [Cat Dog]
  "The cat hisses at the dog.")
```

## Polymorphism vs. Encapsulation

Encapsulation is hiding implementation details from those depending on our code.
This is good, because implementation details often change, and we would like to
be able to prevent clients from depending on things that are likely to change,
as well as prevent them from invalidating the state of our objects.

Java implements encapsulation using the `private`, `public`, and `protected` keywords.
These keywords tell the compiler who can use a given field/method of a class.
A `private` variable, for example, is only accessable within the class in which
that variable is defined.

```java
public class Cat {
  private String name = "Bella";
}

Cat bella = new Cat();
cat.name = "Tom"; // Error! name is private.
```

Unfortunately, this means that whenever we want to use polymorphism, we have to
break our class' encapsulation. To write a polymorphic method, we need to override
a method defined in a superclass. Our method will therefore have access to all
of the class' private variables and methods, even if it does not need them.

```java
public class Cat {
  private String name = "Bella";

  @Override
  public String interact(Cat otherCat) {
    name = "Sylvester"; // Rename this cat
    otherCat.name = "Tom"; // Rename the other cat
  }
}
```

Java makes us choose between polymorphism and encapsulation.

## The Expression Problem

A great measure of how well a programming languages supports polymorphism
is how well it deals with the _expression problem_.
Here's how Philip Wadler, who coined the term, explains it,

> The expression problem is a new name for an old problem. The goal is to
> define a datatype by cases, where one can add new cases to the datatype and
> new functions over the datatype, without recompiling existing code, and while
> retaining static type safety (e.g., no casts).

Object Oriented code makes it easy to add new cases to the data type. All you
need to do is add a new subclass, and implement all the superclass' methods.
Unfortunately, object oriented code makes it hard to add new functions. To add
a new method to our `Animal` example, you would need to add that method to
every `Animal` subclass, changing their code and causing all of their clients
to recompile.

This is the opposite of the situation in functional programming. Functional
programing makes it easy to add new functions, but adding new data types
requires modifying the code of every function we have already defined.

Modifying existing code to add new functionality violates the _Open/Closed
Principle_, one of the most important principles in software engineering.

A more modern object oriented language that attempts to solve this problem is
_Ruby_. Ruby addresses the expression problem by giving programmers the ability
to _reopen_ classes. In other words, in ruby you can add methods to a class after
it has already been defined,
```ruby
class Dog
  def speak
    "Woof!"
  end
end

spot = Dog.new
spot.speak #=> "Woof!"

#Later...

# Add a new method
class Dog
  def threaten
    "Growl!"
  end
end

spot.threaten #=> "Growl!"
```
