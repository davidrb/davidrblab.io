---
layout: essay
type: essay
title: "Design Patterns are Procrastination"
date: 2020-04-30
labels:
  - Design Pattern
---

Everyone loves design patterns. But why are they so useful? And what makes a
good design pattern (as opposed to an anti-pattern)? In my opinion, a good
design pattern is one that lets you procrastinate making decisions.

## Procrastinating Decisions is a Good Thing

Have you ever designed an API, and only realized it was a poor design
after you used it hundreds of times throughout your program?
You probably thought it was a good decision at the time, hindsight is
always 20/20.

But what if you could wait until you have this hindsight to make your
design decisions? It's not magic, it's design patterns.

## Creational Patterns

One of the most common tasks of an Object Oriented programmer is instantiating
objects. But one of the core features of Object Oriented programming is
polymorphism, which lets different kinds of objects implement the same
interface. So which concrete class should you instantiate?

The answer is: procrastinate! Decide which class to instantiate later by
using a _Factory_. Instead of creating those objects in 100 different
places throughout your codebase,

```
class MyClass {
  someMethod() {
    let foo = new Foo();
    //...
    let bar = new Foo();
    //...
    let baz = new Foo();
    /...
  }
}
```

You can use dependency injection to inject a factory:

```
class MyClass {
  constructor(factory) {
    this.fooFactory = fooFactory;
  }

  someMethod() {
    let foo = fooFactory.createFoo();
	 //...
    let bar = fooFactory.createFoo();
	 //...
    let baz = fooFactory.createFoo();
	 //...
  }
}
```

Now you can implement MyClass before you decide what kind of Foos you want to
instantiate.

## Structural Patterns

Let's say you have two different APIs that do the same thing.  For example,
OpenGL and DirectX to render hardware accelerated 3D graphics. What if you
can't decide which to use? Or what if you need to support both?

Procrastination saves the day again! You can use the [Adapter Pattern](https://en.wikipedia.org/wiki/Adapter_pattern)
to procrastinate your decision.

## Anti Patterns don't Let You Procrastinate

One "design pattern" that is often considered to be an "anti-pattern" is the
Singleton design pattern. It's no coincidence that singletons don't let you
procrastinate decisions the same was other design patterns do.  Before reaching
into you bag of design patterns, its worth asking yourself "How is this design
pattern is helping me procrastinate?" If you can't think of a good answer you
may be using an anti-pattern or misuing a design patter.

