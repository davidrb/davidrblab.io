---
layout: essay
type: essay
title: The Best of Functional Programming Meets the Worst of Object Oriented Programming
date: 2020-1-23
labels:
  - JavaScript
  - Java
  - Programming Languages
---

One of the essential steps in mastering any programming language is to learn
the language's "story." By story I mean things like who designed it? When and Why? What
languages did the designers draw inspiration from? Why did they choose the features they did and not others?
And sometimes, _"what the hell were they thinking?"_

Most of the time, the best way to use a tool is the way it was designed to be
used, and even a brief look into the history of JavaScript reveals that it was
designed to be functional, not object oriented.

JavaScript was designed by Brendan Eich while working for Netscape. His
original goal was to embed Scheme into the Netscape Navigator web browser.
However, Netscape was partnered with Sun Microsystems, the creators of Java, so
they wanted Eich to make a language that would complement Java, syntax wise. If
JavaScript were a comic book villian, this would be its origin story.

The marrying of two very different programming languages had many consequences
for JavaScript, some good and some bad.

## First Class Functions

JavaScript doesn't discriminate against functions. Just like there is
a type for strings, arrays, and numbers, JavaScript has a type for functions.

```
typeof(function(){})
'function'
```

This construct is extremely powerful, and can be extremely elegant. It is one
of JavaScript's best features.

## Expressions vs. Statements

A statement is a syntactic construct that represents an _action_ to be carried
out. An expression is a syntactic construct that has a value.

An if statement is a statement (shocker). You cannot write `let a =
if...` in JavaScript. The syntax of JavaScript, like that of Java, is heavily
statement oriented.

This is by far one of JavaScripts biggest downsides. Functional languages deal
inherently with _values_, not imperative _actions_. Expressions are, by
definition, how you express values.

There are, however, a few instances where Scheme won over Java in this regard.
One example are function expressions. The following two ways of defining a
function are almost identical.

```
function foo() { /*...*/  };
let foo = function() { /*...*/ };
```

## References

Consider the following function, that tests if a value is present in an array.

```
let contains = function(val, arr) {
  return -1 !== arr.indexOf(val);
}
```

This function works:

```
contains(2, [2, 4, 6]); // => true
contains(3, [2, 4, 6]); // => false
contains({}, [{}]); // => false
```

Wait, what's wrong with the last one?

Nothing. Our function compares values. In JavaScript, `{}` is an expression
who's value is a _reference_ to an object. Unfortunately, each `{}` is a
different object, therefore the values are different.

People often talk about C++ pointers being complicated.  Despite what many
people think, these JavaScript references behave much like C++ pointers. The
main difference is that C++ supports pointer arithmetic.

Java and JavaScript are often mistaken as "pass by reference" languages.
They're not. If you don't understand this, think about what it means to pass a
reference by value.

And if you think this is confusing, you're right.

## The `this` Keyword

One of the most "interesting" "features" you get when you collide Java and
Scheme inside a partical accelerator is the `this` keyword.

In JavaScript, when you write `foo.bar()`, the expression `this` is made to
reference `foo` while `bar` is being executed. In other contexts, `this` is
made to refer to some global object.


The following works as you probably expect:

```
let o = {
  x: 3,
  f: function() { return x; }
};

o.f(); //=> 3
```

But the following might not:
```
let o = {
  x: 3,
  f: function() { return (function(){return this.x;})(); }
};

o.f(); //=> undefined.
```

The second example shows that the value of `this` is not captured in a lambda
expression the way other values are. This is just one way the `this` keyword
can lose context and lead to very frustrating debugging.

## Conclusion

JavaScript is JavaScript and we're stuck with it at least for the time being.
But understanding why it is the way it is will help any JavaScript developer
make better use of it.
