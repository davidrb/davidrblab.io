---
layout: essay
type: essay
title: What Coding Standards Miss
date: 2020-2-13
labels:
  - Programming
---

<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
The implementation of coding standards is a common software development
practice designed to improve code quality. To a certain extent, they achieve
this. However, coding standards alone are far from enough to guarantee code is
high quality. This begs the question, how and why do coding standards fall
short, and what other software development practices are required to ensure
high quality code in these cases?

Coding standards are by definition a set of rules source code is to conform to
that are to be checked automatically. But there are some rules that can't be
checked automatically. Software development is all about making tradeoffs, and
using the right tools or features to solve the problem.  There is no way eslint
or checkstyle can enforce "Prefer composition over inheritance."

A student once asked me and others for help doing an assignment. The assignment
was to determine whether or not to allow a person to board a flight, given
whether the flight was international, whether the passanger has a boarding
pass, and what forms of identification the passanger posseses. The student had
written dozens of lines of nested if statements, with each branch setting a
boolean value to true or false. It looked something like

```java
if (!hasBoardingPass) {
  allowed = false;
} else if (internationalFlight) {
  if (hasPassport) {
    allowed = true;
  }
} else {
  if (hasDriversLicense || hasPassport || hasMilitaryID) {
    allowed = true;
  }
}
```

When I pointed out to him that he could solve the problem without writing a
single if statement, he, along with some other students, were slightly
incredulous. So I showed them,

```java
boolean hasId = hasDriversLicense || hasPassport || hasMilitaryID;
boolean sufficientId = internationFlight ? hasPassport : hasId;

allowed = hasBoardingPass && sufficientId;
```

To be fair, there were many problems with his code that would have been
immediately corrected by using coding standards. But no set of coding standards
will be enough to transform the first code into the latter. The limitations of
coding standards should be understood, so we know how and when to use other
software software development practices along with them.
