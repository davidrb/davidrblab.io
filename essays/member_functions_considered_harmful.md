---
layout: essay
type: essay
title: Member Functions Considered Harmful
date: 2018-12-09
labels:
  - Software Engineering
---

Often, when designing the interface to a class, we have to decide whether to
implement a certain function as a 'member function',

```cpp
class Foo {
public:
  virtual void bar() {
    // ...
  }
};
```
or as a 'free function'.

```cpp
class Foo {};

void bar(Foo& foo) {
  // ...
}
```

Many people would consider the member function approach more 'object oriented',
and therefore better. In reality, free functions are not only simpler, they are also
**more** object oriented than member functions, and programmers should prefer them whenever
possible.

Programmers should only use member functions to implement the class' *minimal complete interface*.
All Other functionality should be free functions, implemented in terms of the
minimal complete interface.

## Free Functions are more Encapsulated

To understand why free functions are more object oriented, it helps to understand what the difference
between the two approaches are. A member function, such as the one above, gets compiled
into something like

```cpp
void Foo::foo(Foo *this) {
  // ...
}
```

which is very similar to the free function version, the most important difference
being that *the member function has access to the class' private implementation details.*
This is important because, assuming the class' member functions are implemented correctly,
the free function cannot leave the object in an invalid state. A member function can.
This concept, called **encapsulation** is one of the cornerstones of object oriented programming.

## Free functions are more SOLID

Bob Martin created the SOLID mnemonic to help programmers remember 5 important principles
of object oriented design. Free functions are at least as good as member functions when
it comes to each of the five SOLID principles. There are two principles where free functions
are clearly superior to member functions.

- **O**pen-closed Principle
  The Open-closed principle states that a class should be "open for extension, but closed for modification."
  In other words, you should be able to extend the functionality of a class without modifying
  the class' source code.

  Adding functionality to a class as a member function requires you to change the definition
  of that class. On the other hand, you can add as many free functions as you want without
  affecting the class definition at all.

- **I**nterface Segregation Principle
  The Interface Segregation principle states that "one client-specific interface is better than
  many general-purpose interfaces." Implementing all of the functionality of a class as member
  functions forces the class to have one general purpose interface for all of the clients of your
  class. When functionality is implemented as free functions, those functions can be split up into
  many different header files, and clients can include only the ones they need.

## When to use Member Functions

Unfortunatley, not all of a class' functionality can be implemented as free functions.
As discussed above, free functions don't have access to a class' private implementation details
(unless it's friended).
If a function *needs* access to these (if they can't be implemented in terms of other functions
that do), then that functions must be implemented as a member function.

Another possible use case for member functions is to implement polymorphic behanviour.
Not all polymorphism must be implemented as member functions, however.
Free functions can call other polymorphic member functions (ones that are part of the class'
minimal complete interface.) to implement polymorphic behaviour.
Still, some polymorphic functions will have to be implemented as member functions.

Finally, if you're using a language like Java, you can't write free function. Oof.
