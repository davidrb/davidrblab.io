---
layout: essay
type: essay
title: What is a Framework, and why are Libraries Better?
date: 2020-2-27
labels:
  - Programming
---

# What is a Framework, and why are Libraries Better?

Frameworks are all the rage these days. If you look up jobs for software
developers, you'll see listing after listing looking for an "X Developer,"
where X is the name of some framework.

But what is a framework anyway? Lots of people seem to think they're some kind
of library on steroids. In my opinion, frameworks suck and we should be writing
libraries instead.

To understand why, you need to understand how to write a computer program,
and for that, you need to understand what a programming language is.

## Programming Languages

There is, fundamentally, only one way to write a computer program. This is a corollary of what a programming language _is_.
As explained by Sussman and Abelson, there are three components to any programming language. These are its',

- Primitive Elements
- Means of Combination
- Means of Abstraction

The primitive elements are things like JavaScript's data types and operators.
They are the raw materials a programmer uses to build their programs.

Unless the problem you are trying to solve is trivial, the primitive elements
arent enough. All you can do now is _combine_ them together to produce more
complicated and, hopefully, more useful behaviour.

If your language is Turing complete, this might be all you need to solve any
problem a programming language can possibly solve. However, if you are human,
there is also a (rather tiny) limit to the amount of complexity you can handle.

This is where the final, and most important piece of the puzzle comes into
play. The means of abstraction are a way of "giving a name" to a combination of
primitive elements, so that the combination can be treated as if it were also a
primitive element. Abstractions can also represent a class of different
combinations that do the same general sort of thing, or the same thing in
different ways, allowing you to "swap out" one "instance" of an abstraction
with another.

Programming is simply the art of combining primitive elements to increase
complexity, and then creating abstractions to reduce complexity.

## Frameworks vs. Libraries

A Library is a collection of abstractions that have been "packaged up" together
to be reused in other programs. Frameworks are more like a "skeleton" that you
"flesh out" with your own code.

Consider a program that has some form of persistant state (like a database).
The user requests the program to possibly take some action, which may or may
not change the state, and respond with some data.

If you are a modern software developer, you may think I've just decribed a web
application.  The truth is, what I just described is so much more general than
a web application. What I really described was an _abstraction_ of an
application's behaviour. Once you _combine_ an instance of this abstraction
with a means of transfering requests and responses between it and the user, you
get a bona fide application. If this means of transferring requests happens to
be html/css/javascript over HTTP, then you have a web application.

Unfortunately, these things are often conflated in not only people's minds, but
also in their code. The reason, I believe, is the popularity of "web
frameworks." People like the structure provided by frameworks, however I
believe this benefit is illusory. Frameworks, unlike libraries, do not act like
primitive elements of a language that can be combined together and abstracted.
The code you write is not combined with a framework in a general way, but in a
very specific way that is usually not under your control. And good luck trying
to combine two frameworks together.
