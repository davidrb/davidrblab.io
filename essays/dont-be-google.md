---
layout: essay
type: essay
title: "Don't be Google"

date: 2020-04-23
labels:
  - Privacy
---

## Ethics in Software Engineering

In my opinion, ethics refers to a set of principles designed to lead to
behaviour that is morally good, and which discourages behaviour that is morally
bad. Different disciplines have their own set of ethics, for example, Lawyers
have a specific set of rules regarding ethical behaviour they must follow or
face sanctions or loss of their license to practice law.

Software development also has ethics which, while not as formally defined as
those in law, may be as important, given the power software has in the modern world.

I believe that the world of software and technology is doing a uniquely poor
job at promoting ethical behaviour. This is disturbing, considering the top 3
companies in the world (in terms of market capitalization) are software
companies. These companies have enormous power, and with power, comes the risk
of unethical behaviour.

## Google Street View

In 2010, Google was caught collecting personal information from unsecured wifi
networks using their Google Street View cars, in a process similar to "war
driving," where hackers drive around looking for unsecured wifi networks to
attack. This information included not only information that would be useful for
improving their location based services, such as Google Maps, but also medical
and financial records, passwords, and email content.

Google initially denied such data was collected, later conceded that only
'fragments' may have been collected, and finally admitted that such data had
been not only collected, but stored.

## Why you shouldn't trust Google's Explanation

Google's claim was that they accidentally included software designed to improve
the accuracy of their location based services, such as Google Maps. But even if
deploying this software was an accident, the fact remains that they were
developing it in the first place. This, along with Google's efforts in the
courts to legally support data collection from unsecured wifi networks shows
that collecting this data was Google's goal all along.

Additionally, the fact that the software was designed to differentiate between
secured and unsecured wifi networks shows that Google is being dishonest. Wifi
uses a protocol known as 802.11. In 802.11, wifi routers adverstise their
presence by broadcasting "Beacon Frames." All 802.11 frames include the SSID and
BSSID of the WAP, which in practice is the MAC address of the WAP's NIC. If
Google wanted to collect data to make Google Maps more accurate, they would
just need to listen to these Beacon frames, and it would not matter if the wifi
networks were encrypted or not.

## Honesty

The Question everyone needs to ask themselves is "If improving the accuracy of
Google Maps wasn't Google's goal when writing their packet sniffing software,
what was?"

In my opinion, Google violated more than a few ethical principles software
developers should hold themselves to. However, the principle that they, and
other software companies, violate most often and most egregiously is "Be honest
and trustworthy." They do this by misrepresenting themselves to their users (as
opposed to their customers). It is dishonest for a company to claim to care
about Privacy when its entire business model is antithetical to privacy.

### Case in Point: YouTube

One of Google's most popular products is the video hosting platform YouTube.
But despite being enourmously popular, it is commonly believed that YouTube
does not make money. Why would Google put so much money and effort into running
YouTube if it does not make them any money?

Most YouTube users will agree that one of the biggest, if not the biggest,
issues with YouTube is abuse of the Digital Millenium Copyright Act and
YouTube's comically flawed content ID system. YouTube and its CEO have made
promises in the past to fix this issue, but they haven't delivered. How can a
website be successful when they have no regard whatsoever for what their users
want?

The answer to both questions is that the purpose of YouTube is not to host
videos and give people a means to "broadcast themselves," its purpose is to
show people targeted advertisments. Similarly, the purpose of Google street
view and Google Maps is to collect data to build profiles of users to more
effectively target ads for them, by tracking their movements to find out what
stores and restaraunts they frequent.

If Google were honest and upfront about these facts, then it might be possible
to defend them. But instead they hold themselves out as people who care about
our privacy. Google is telling their users "We want to protect your privacy"
when they should be telling them "We make money by intruding on your privacy,
specifically by doing x, y, and z," so they can decide for themselves if it's
worth using their software.

"Look like the Good Guy" is not an ethical principle software companies should follow.
If you're not the Good Guy, don't pretend to be.

One of the ethical principles Lawyers are expected to follow is to disclose
conflicts of interest to their clients. If Google were held to the same
standard, they would have lost their software license long ago.  They certainly
haven't lived up to their motto of "Don't be evil."
