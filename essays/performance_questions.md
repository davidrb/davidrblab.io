---
layout: essay
type: essay
title: How to Ask Performance Questions
date: 2020-1-30
labels:
  - Stack Overflow
---

There are a few things to keep in mind when asking about the performance of
code online.

One of the most popular Stack Overflow questions (with over 24k upvotes as of
this writing), is
["Why is processing a sorted array faster than processing an
unsorted array?"](
https://stackoverflow.com/questions/11227809/why-is-processing-a-sorted-array-faster-than-processing-an-unsorted-array)
This is an example of asking a question the "smart way."


The question is why processing a large, randomly generated array in a certain
way is six times faster when the array is sorted vs when it is not sorted.
The question is non-trivial and interesting, and led to some very good answers.

Not all questions are "smart," however.
[This asker](https://stackoverflow.com/questions/17686749/javascript-performance-conditional-statement-vs-assignment-operator)
wonders whether there is a significant speed difference between writing

```
this.crm.isUpToDate = false;
```

and writing

```
if (this.crm.update === true) {
    this.crm.isUpToDate = false;
}
```

This question did not receive the same positive reception as the "smart" question.

## Measure It

The asker of the "smart" question measured the performance of the code he was
asking about.

```
Without std::sort(data, data + arraySize);, the code runs in 11.54 seconds.
With the sorted data, the code runs in 1.93 seconds.
```

In fact, profiling his code is what led to his question in the first place.
Performance, especially on modern hardware with caching and branch prediction,
or on modern time sharing operating systems, is very often unintuitive.
Therefore, performance critical code should _always_ be measured, not only when
asking about it online.

The asker of the "not smart" question did not profile his code. If he
did, there would be no reason to ask his question in the first place. He is
essentially just asking other people to do his job for him.

Profiling performance also has the benefit of helping to prevent the following
mistake.

## Don't Prematurely Optimize

Donald Knuth famously held premature optimization to be the root of all evil.
Premature optimization is spending lots of time and effort optimizing
things that have no chance of making a significant difference.

If a line of code makes up less than 1% of the running time of your program,
then even if you somehow managed to optimize it away completely, your program
would barely speed up by 1%. If there is any noticable performance difference,
it will be negative.

The "not smart" question is the epitome of premature optimization. He says he
is working on a database, which is going to be much slower than assigning
booleans in JavaScript.

The "smart" questions was asking about a performance difference of six times,
which is a significant difference.

## Don't Ask People to Make Your Code Faster

[This asker](https://stackoverflow.com/questions/16702402/faster-code-anyone),
in addition to being annoying, just straight up asks people to make
his code faster. If he wants someone to write his code for him, he should hire a programmer.
