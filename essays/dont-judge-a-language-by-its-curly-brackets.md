---
layout: essay
type: essay
title: Don't Judge a Language by its Curly Brackets
date: 2018-05-09
labels:
  - Software Engineering
  - C++
  - Java
  - JavaScript
---

It's common to see programming languages that look similar, superficially, but
are in reality completely different. C++, Java, and JavaScript are a prime
example.  Believe it or not, the following three functions/methods do
completely different things:

```cpp
bool check_password(string s) {
  return s == "1234";
} // C++
```
```java
public boolean checkPassword(String s) {
  return s == "1234";
} // Java
```
```javascript
function checkPassword(s) {
  return s == "1234";
} // JavaScript
```

Understanding how and why these code samples differ can provide insight into
how these languages work, what tradeoffs were made in their design, and how to
use them more effectivley.

## The C++ Version

The C++ code does what one would naively expect it to do. It returns true if
and only if the string object passed to the function is equivalent to the
string `"1234"`. In fact, the C++ code is the only code above that works 100%
correctly. There are still a few things to note about this code, however.

### Pass By Value vs. Pass By Reference

The `s` parameter is passed to this C++ function by value. This means that
before the function body executes, the string s will be copied. While probably
not a big deal in this example, depending on the length of the string, this
copy could in principle be very expensive in terms of both time and memory.

Pass by reference makes certain C++ code impossible to write in languages that
don't support it.  For example, the following swap function works in C++, but
not in Java.

```cpp
template<typename T>
void swap(T& a, T& b) {
  T c = a;
  a = b;
  b = c;
} // works!
```

```java
void swap(Object a, Object b) {
  Object c = a;
  a = b;
  b = c;
} // doesn't work!
```

Meanwhile, the following C++ swap function doesn't work, because the parameters
are passed by value rather than by reference.

```cpp
template<typename T>
void swap(T a, T b) {
  T c = a;
  a = b;
  b = c;
} // doesn't work!
```

### The Better Version

A better version of the C++ code is the following:
```cpp
bool check_password(string const& s) {
  return s == "1234";
}
```

The `const&` signifies that the parameter is to be passed to the function as a
const reference. This means that the `s` in the function body refers to the
actual `string` object passed into the function, not a copy of it. The `const`
tells the compiler that any attempt to modify the original string object should
be treated as an error. This helps avoid accidentally modifying parameters that
were passed into our function.

### C++ Takeaways
- Use `operator==()` to compare _values_.
- To compare object _identities_, compare their addresses: `&foo == &bar`
- Pass a parameter as a reference when you need to modify it.
- Pass a parameter by value or const reference if you don't plan on
  modifying it.
- Watch out for expensive copies when passing parameters by value.

## The Java Version

Perhaps surprisingly, the Java code give above is incorrect.  It might _appear_
to work sometimes, but other times it will fail.

### Value vs. Identity Comparison

In the above Java code, `s` and `"asdf"` are _references_ to `String` objects.
In Java, `==` compares the value of the _references_, not the value of the
referred objects. In other words, it compares the _identity_ of objects rather
than the _value_ of objects.

The Java code above is actually more similar to the following C++ code than the
above C++ code:

```cpp
bool check_password(string *s) {
  return s == "1234"; // Always false
}
```

Unfortunately, the above code will appear to work intermittantly.
Java will usually cache literal strings, so the following scenario is likely:

```java
checkPassword("1234"); // => true
checkPassword(new String("1234")); // => false
```

### The Correct Version

We fix the above Java code by using the `.equals(Object o)` method:
```java
public boolean checkPassword(String s) {
  return s.equals("1234");
}
```

### Java Takeaways
- Use `==` to compare object _identities_, `.equals` to compare object _values_.
- Swapping variables in Java is annoying.

## The JavaScript Version

Despite what their names suggest, the JavaScript code is closer to the C++ code
than the Java code in terms of correctness. In JavaScript, `==` compares
Objects similarly to Java, but JavaScript strings are considered primitive
types, rather than Objects, so `==` compares values of strings. There is at
least one quirk in the above JavaScript code, which in some cases can lead to
impossible to find bugs and massive security flaws.

### Type Coercion

The JavaScript `==` operator performs \(rather liberal\) _type coercion_.
We expect our `checkPassword` function to return true if and only if we
pass in a string with the value `"1234"`. This assumption is not true, as shown below:

```javascript
checkPassword(1234); // => true
```

`checkPassword` returns true even though the value we passed in wasn't a
string.  That's because the `int` gets _type coerced_ into a string before it
is compared.

### The Correct Version

To fix the JavaScript code, we use the _strict_ equality operator `===`, which
does not perform type corecion.

```javascript
function checkPassword(s) {
  return s === "1234";
}
```

### JavaScript Takeaways
- strings are primitive types, not Objects
- Watch out for type coercion
- Only use the strict equality operator `===`

## Don't Fall For It!

With any luck, this essay will end the practice of language designers
disguising their languages as C in order to attract programmers more
comfortable with those languages.  I'm not going to hold my breath, though.

![https://coding.smashingmagazine.com/2009/07/misunderstanding-markup-xhtml-2-comic-strip/](https://i.stack.imgur.com/TsJyw.jpg)

[source](https://coding.smashingmagazine.com/2009/07/misunderstanding-markup-xhtml-2-comic-strip/)
