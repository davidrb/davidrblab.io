var angles = [];
var rs = 0.01;
var r0 = 1.0;

for (var i = 0; i <= 4096; i++) {
  var r = r0;
  var phi = Math.PI;
  var theta = (i*180/4096) * (Math.PI/180);

  var sin_theta = Math.sin(theta);
  var cos_theta = Math.cos(theta);

  var r_dot = -Math.sqrt(1.0-rs/r)*cos_theta;
  var phi_dot = -(1.0/r)*sin_theta;
  var phi_dot0 = phi_dot;

  var d_tau = 0.001;
  var d_tau2 = d_tau*d_tau;

  var bad = false;

  do {
    var k = 1-rs/r;
    var t_dot2 = Math.abs(r_dot*r_dot/k/k + r*r/k*phi_dot*phi_dot);

    var r_ddot = 0.0;
    r_ddot += -rs*r_dot*r_dot/(rs-r);
    r_ddot += rs*t_dot2*(rs-r)/r/r;
    r_ddot += -2*phi_dot*phi_dot*r*(rs-r);
    r_ddot /= 2*r;

//    var phi_ddot = -2*r_dot*phi_dot/r;

    r_dot += r_ddot * d_tau;
//    phi_dot += phi_ddot * d_tau;
    phi_dot = (r0/r)*(r0/r)*phi_dot0;

    r += r_dot*d_tau;
    phi += phi_dot*d_tau;

    if (r < rs) {
      bad = true;
      break;
    }
  } while ((Math.abs(phi_dot) >= 0.1) || (r_dot < 0));

  while(phi >= Math.PI+0.1) {
    phi -= Math.PI*2;
  }
  while(phi < -Math.PI-0.1) {
    phi += Math.PI*2;
  }

  if(bad) {
    angles[i] = 0.0;
  } else {
    angles[i] = phi;
  }
}

(function() {
  var canvas = document.createElement('canvas');
  var renderTarget = document.getElementById("webgl");

  canvas.width = Math.min(window.innerHeight, window.innerWidth)/2;
  canvas.height = Math.min(window.innerHeight, window.innerWidth)/2;
  //canvas.width = 400;
  //  canvas.height = 400;

  renderTarget.appendChild(canvas);

  gl = canvas.getContext('webgl');


    var floatTextures = gl.getExtension('OES_texture_float');
    if (!floatTextures) {
        alert('no floating point texture support');
        return;
    }

    if(!gl.getExtension('OES_texture_float_linear')) {
      alert('no linear');
      return;
    }

  gl.clearColor(1.0, 0, 1.0, 1);
  gl.clear(gl.COLOR_BUFFER_BIT);

  var vertexShader = gl.createShader(gl.VERTEX_SHADER);
  gl.shaderSource(vertexShader, [
      'attribute vec2 position;',
      'attribute vec2 uv;',

      'varying vec2 frag_uv;',

      'void main() {',
        'frag_uv = uv;',
        'gl_Position = vec4(position, 0.0, 1.0);',
        '}'
  ].join('\n'));
  gl.compileShader(vertexShader);

  var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
  gl.shaderSource(fragmentShader, [
      'precision highp float;',
      'varying vec2 frag_uv;',
      'uniform sampler2D texture;',
      'uniform sampler2D angles;',
      'uniform float r;',

      'void main() {',
        'float sin_theta = length(frag_uv - vec2(0.5, 0.5))*sin(70.0*3.141592/180.0);',
        'float theta = asin(sin_theta);',
        'float phi = texture2D(angles, vec2(theta/3.141592, 0.5)).x;',
        'vec2 f = vec2(0.5, 0.5) + sin_theta/sin(phi)*(frag_uv-vec2(0.5, 0.5));',

        'if (phi == 0.0)',
          'gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);',
        'else',
          'gl_FragColor = texture2D(texture, vec2(f.x, r+1.0-f.y));',
        '}'
  ].join('\n'));
  gl.compileShader(fragmentShader);
  var compiled = gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS);
  console.log('Shader compiled successfully: ' + compiled);
  var compilationLog = gl.getShaderInfoLog(fragmentShader);
  console.log('Shader compiler log: ' + compilationLog);

  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);


  var vertices = new Float32Array([
      -1.0, -1.0, 0.0, 0.0,
      1.0, -1.0, 1.0, 0.0,
      -1.0, 1.0, 0.0, 1.0,
      -1.0, 1.0, 0.0, 1.0,
      1.0, 1.0, 1.0, 1.0,
      1.0, -1.0, 1.0, 0.0
  ]);

  var vertex_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  program.position = gl.getAttribLocation(program, 'position');
  program.uv = gl.getAttribLocation(program, 'uv');
  program.r = gl.getUniformLocation(program, 'r');

  var texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);
  // Fill the texture with a 1x1 blue pixel.
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
      new Uint8Array([255, 0, 0, 255]));
  // Asynchronously load an image
  var image = new Image();
  image.src = "/images/texture6.jpg";

  image.addEventListener('load', function() {
    // Now that the image has loaded make copy it to the texture.
    gl.bindTexture(gl.TEXTURE_2D, texture);
    console.log(gl.getError());
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,gl.UNSIGNED_BYTE, image);
    gl.generateMipmap(gl.TEXTURE_2D);
    console.log(gl.getError());

    console.log("loaded");

    gl.useProgram(program);
    gl.uniform4fv(program.color, [0, 0, 1, 1.0]);

    gl.enableVertexAttribArray(program.position);
    gl.enableVertexAttribArray(program.uv);
    gl.vertexAttribPointer(program.position, 2, gl.FLOAT, false, 4*4, 0);
    gl.vertexAttribPointer(program.uv, 2, gl.FLOAT, false, 4*4, 2*4);

    var r = 0.0;
    var x = [];
    for (var i = 0; i < 4096; i++) {
      x[i*4] = angles[i];
      x[i*4 + 1] = 0.0;
      x[i*4 + 2] = 0.0;
      x[i*4 + 3] = 0.0;
    }

    console.log(x);


    var angles_array = new Float32Array(x);
    gl.activeTexture(gl.TEXTURE1);
    var angles_texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, angles_texture);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 4096, 1, 0, gl.RGBA, gl.FLOAT, angles_array);

    program.angles = gl.getUniformLocation(program, "angles");
    gl.uniform1i(program.angles, 1);


    function update(progress) {
      r += 0.003;
    }

    function draw() {
      gl.uniform1f(program.r, r);
      gl.drawArrays(gl.TRIANGLES, 0, vertices.length/4);
    }

    function loop(timestamp) {
      var progress = timestamp - lastRender

        update(progress)
        draw()

        lastRender = timestamp
        window.requestAnimationFrame(loop)
    }
    var lastRender = 0
      window.requestAnimationFrame(loop)

  });
      })();
