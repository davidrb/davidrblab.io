---
layout: project
type: project
image: images/musicbuds.png
title: ManoaMusicBuds
permalink: projects/manoamusicbuds.html
date: 2020
labels:
  - Web
  - JavaScript
  - React
  - Meteor
summary: We developed an application to help people find fellow musicians and bands to play with.
---

For our ICS314 Final Project, my team and I create ManoaMusicBuds.
ManoaMusicBuds is a web application that helps musicians connect with
eachother, form bands, and plan and attend performances.

Musicians can sign up and fill out a biography about themselves, which includes
what instruments they play and what genres of music they enjoy playing.  Users
can view a list of all other users, and filter them based on which instruments
they play, and what generes of music they are interested in playing.

<img alt="Listing all users" style="width: 100%" src="https://manoa-musicbubs.github.io/images/5.png" />

ManoaMusicBuds also shows a list of bands that our users have formed. Musicians
can start bands, and other musicians can apply for poistions in those bands.
Finally, ManoaMusicBuds show a list of events, such as concerts. Future events
are listed in the upcoming events page.

My main contribution to the project was implementing the bands feature, and the
Upcoming Events page.  I also implemented a responsive NavBar so that the site
worked better on mobile.

Read more at [the website](https://manoa-musicbubs.github.io/).
View our code at [our github repo](https://github.com/manoa-musicbubs/manoa-musicbuds-source).
