---
layout: project
type: project
image: images/washndash.png
title: Wash-n-Dash
permalink: projects/wash_n_dash.html
date: 2018
labels:
  - Web
  - JavaScript
  - React
  - Meteor
  - Raspberry Pi
summary: We developed an application for UH Manoa students to track and update the status of washers and dryers in the dorm's laundry room.
---

<figure style="max-width: 50%;" class="ui medium right floated rounded image">
  <img src="/images/admin_page.png" />
  <figcaption>The Admin Page</figcaption>
</figure>

For our ICS314 Final Project, we developed a web application to help UH Manoa
students track available washers and dryers in the dorm's laundry room. The
system consists of a web based frontend to display available washers and dryers
and a Raspberry Pi based sensor with a gyroscope that detects when washing
machines/dryers are in use.

I, along with the rest of the team, developed the front end in javascript
using Reactjs version of Semantic-ui. The application was developed using
Meteorjs, so the backend was also JavaScript, and used the MongoDB NoSQL
database.

I developed the Raspberry Pi sensor. The sensor consists of a Raspberry Pi 3,
connected to a gyroscope over SPI. When it detects vibration, the Pi measures
the gyroscope and sends a POST request to a REST endpoint I exposed on the
Meteor backend.

<!-- You can view the deployed app [here](http://wash-n-dash.meteorapp.com/#/). -->
Our code is available at the [github
repo](https://github.com/wash-n-dash/wash-n-dash).
