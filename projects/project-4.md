---
layout: project
type: project
image: images/bloch_sphere.png
title: Quantum Computing Presentation
permalink: projects/quantum_computing_presentation.html
date: 2018
labels:
  - Quantum Computing
  - Physics
summary: I gave a presentation on quantum computing for my ICS 623 class.
---

<figure style="width: 100%;" class="ui medium center rounded image">
  <img src="/images/quantum_teleport.png" />
  <figcaption>The Quantum Teleportation Circuit</figcaption>
</figure>


I gave a presentation on the basic theory behind quantum computing for my ICS
623 class. The first part of the presentation consisted of the basic postulates
of Quantum Mechanics, the second part explained the basics of qubits, Quantum
gates and Quantum circuits, and the presentation ended by showing two
interesting applications: Quantum teleportation and super-dense coding.

The audience consisted of the professor, [Dr. Depeng
Li](https://www2.hawaii.edu/~depengli/), as well as the other students. You can
view my slides
[here](https://gitlab.com/davidrb/quantum-computing-presentation/blob/master/quantum_computing/presentation.pdf).
