---
layout: project
type: project
image: images/opq.jpg
title: OpenPowerQuality
permalink: projects/opq.html
date: 2015
labels:
  - Embedded
  - KiCad
  - Raspberry Pi
  - STM32
  - Arm
  - ZeroMQ
  - C++
summary: We are developing distributed power quality meters to study the effect of renewable energy on the electrical grid.
---

<figure style="max-width: 50%;" class="ui medium right floated rounded image">
  <img src="http://openpowerquality.org/img/opqbox_photo.jpg" />
  <figcaption>OPQBox 2.6</figcaption>
</figure>

Open Power Quality aims to develop an open source, distributed, power quality sensor network across the island of
Oahu for the purpose of studying the effect of distributed renewable energy sources on the grid.

I spent my first semester on the project writing firmware for the 32-bit Arm Digital Signal Processor on board the OpqBox.
The DSP's job was to measure the instantaneous voltage across the mains with its built in ADC, calculate the instantaneous
frequency, and send this data to the Rasperry Pi via SPI.

Next semester, I used the ZeroMQ library to develop the infastructure we need to enable the collection of power quality data
from our sensors. I wrote the aquisition broker used by the boxes to send their data to our servers in a distributed manner.
The broker provides a fixed intermediary between the boxes and our servers, and allows boxes and servers to seamlessly connect and disconnect at will.

I then designed the next revision of the hardware in KiCad.  We switched our
design software to KiCad from a proprietary tool, in favor of something open
source. Another goal was to make the design more manufacture-friendly.

You can learn more at the [Open Power Quality website](http://openpowerquality.org).
You can view our code and design files at [our github repository.](https://github.com/openpowerquality/opq)
