---
layout: project
type: project
image: images/redshift.png
title: Black Hole Simulator
permalink: projects/black_hole.html
date: 2017
labels:
  - Physics
  - General Relativity
  - Psuedo-Reimannian Geometry
  - C++
summary: A numerical simulation of a schwarszchild black hole.
---
<figure style="">
  <img src="../images/black_hole.gif" style="max-width: 100%"/>
  <figcaption>An object falling into the black hole.
  The time dilation caused by the gravitational field is apparant.</figcaption>
</figure>

<figure style="max-width: 50%; margin: 0" class="ui medium right floated rounded image">
  <img src="../images/redshift.svg" style="margin: 0"/>
  <figcaption>Redshifting of a photon moving radially outward from the event horizon.</figcaption>
</figure>

In order to help cement my understanding of Albert Einstein's theory of
general relativity, I wrote a numerical simulation of a spacetime around
a black hole. The first part of this project simulates an object freely-falling
in the vicinity of the black hole. The second part calculates the gravitational
redshift of a photon escaping from the black hole.
You can find the code for this project at [the gitlab repo](https://gitlab.com/davidrb/black_hole/).

General Relativity models space time as a kind of surface, called a Manifold.
Manifolds are generalizations of Euclidean space. Unlike Euclidean space, a
spacetime manifold can have curvature. Because of this curvature, there are not
necessarily "straight lines (or curves)" in a manifold like there are in
Euclidean space, rather, there are "geodesics" which represent curves that are
"as straight as possible." It is these curves that represent the orbits of
bodies moving under the influence of gravity.

The curvature of spacetime is influenced by the distribution of matter and
energy in that spacetime, according to the [Einstein Field Equations](https://en.wikipedia.org/wiki/Einstein_field_equations):

\\[R_{\mu \nu} - \frac 1 2 R g_{\mu \nu} + \Lambda g_{\mu \nu} = \frac{8\pi G}{c^4} T_{\mu \nu}\\]

\\(g_{\mu \nu}\\) in the EFEs is the metric tensor, which is a kind of
generalization of the dot product from Euclidean space. Just like in Euclidean
space, \\(g\\) gives the lengths and angles between vectors.

A geodesic is a curve $\gamma$ such that it's tangent vector $\dot\gamma$ is
parallel transported along $\gamma$,

\\[(\nabla_{\dot\gamma}\dot\gamma)^\mu = \ddot\gamma^\mu + \Gamma^\mu_{\epsilon\delta}\dot\gamma^\epsilon \dot\gamma^\delta = 0\\]

For the Levi-Civita connection, the connection coefficients are given by

\\[\Gamma^\mu_{\epsilon\delta} = \frac 1 2 g^{\mu\zeta}\left[\pdv{g_{\zeta\epsilon}}{x^\delta} + \pdv{g_{\zeta\delta}}{x^\epsilon} - \pdv{g_{\epsilon\delta}}{x^\zeta}\right]\\]

The [Schwarzschild metric](https://en.wikipedia.org/wiki/Schwarzschild_metric)
is the static solution to Einstein's field equations around a spherically
symmetric massive body. The nonzero metric components for $\theta = \frac \pi 2$ are
\\[g_{00} =  \left(1 - \frac {r_s} r\right)\quad g_{11} = \left(1 - \frac {r_s} r\right)^{-1}\quad g_{22} =  r^2\\]

